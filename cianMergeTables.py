import os
import xlrd
import sqlite3

folder_msk = 'D:\\Projects\\Python\\CianParser\\data\\Tables_Msk\\'
folder_spb = 'D:\\Projects\\Python\\CianParser\\data\\Tables_Spb\\'

########
# Moscow
########

conn = sqlite3.connect("data//2019.06//cian_moscow_flats.db")
cursor = conn.cursor()
cursor2 = conn.cursor()
# Moscow
cursor.execute("""CREATE TABLE IF NOT EXISTS flats
                  (id text primary key, rooms text, type text, metro text, address text, area text,
                  house text, parking text, price text, phones text, description text, repair text,
                  area_rooms text, balcony text, windows text, toilet text, children_animals text,
                  additional text, zhk text, series text, ceiling text, lift text, chute text,
                  link text)
               """)
conn.commit()

list_tables = os.listdir(folder_msk)

for i in range(0, len(list_tables)):
    print(i)
    wb = xlrd.open_workbook(folder_msk + list_tables[i])
    sh1 = wb.sheet_by_index(0)
    ids = sh1.col_values(0)
    for j in range(1, len(ids)):
        field = sh1.row_values(j)
        if len(field) == 23:
            field.insert(19, '')
        try:
            cursor.execute("INSERT OR IGNORE INTO flats VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                           field)
            conn.commit()
        except sqlite3.OperationalError:
            print("binding issue")
        except sqlite3.ProgrammingError:
            print("one of the fields is missing in the table")

# Delete records not related to the city
moscow_strings = 'Моск%'
cursor.execute("DELETE FROM flats WHERE address NOT LIKE (?)", (moscow_strings,))
conn.commit()

# Delete records of more than 500'000 per month costs (mistakes)
flats = cursor.execute('SELECT id, price FROM flats')
expensive_flats = 0
for k1, flat in enumerate(flats):
    price = int(flat[1].split('.')[0])
    id_ = flat[0]
    if price > 500000:
        print(price)
        expensive_flats = expensive_flats + 1
        cursor2.execute("DELETE FROM flats WHERE id LIKE ?", (id_,))
print(expensive_flats)
conn.commit()

##################
# Saint Petersburg
##################

conn = sqlite3.connect("data//2019.06//cian_spb_flats.db")
cursor = conn.cursor()

# Saint-Petersburg
cursor.execute("""CREATE TABLE IF NOT EXISTS flats
                  (id text primary key, rooms text, type text, metro text, address text, area text,
                  house text, parking text, price text, phones text, description text, repair text,
                  area_rooms text, balcony text, windows text, toilet text, children_animals text,
                  additional text, zhk text, series text, ceiling text, lift text, chute text,
                  link text)
               """)
conn.commit()

list_tables = os.listdir(folder_spb)

for i in range(0, len(list_tables)):
    print(i)
    wb = xlrd.open_workbook(folder_spb + list_tables[i])
    sh1 = wb.sheet_by_index(0)
    ids = sh1.col_values(0)
    for j in range(1, len(ids)):
        field = sh1.row_values(j)
        if len(field) == 22:
            field.insert(19, '')
            field.insert(19, '')
        if len(field) == 23:
            field.insert(19, '')
        try:
            cursor.execute("INSERT OR IGNORE INTO flats VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                           field)
            conn.commit()
        except sqlite3.OperationalError:
            print("binding issue")
        except sqlite3.ProgrammingError:
            print("one of the fields is missing in the table")

# Delete records not related to the city
spb_string_1 = 'Санкт-Петерб%'
spb_string_2 = 'Лен%'
cursor.execute("DELETE FROM flats WHERE address NOT LIKE (?) AND address NOT LIKE (?)", (spb_string_1, spb_string_2))
conn.commit()
# cursor.execute("DELETE FROM flats WHERE address NOT LIKE 'Санкт-Петерб%' AND WHERE address NOT LIKE 'Ленин%'")

# Delete records of more than 500'000 per month costs (mistakes)
flats = cursor.execute('SELECT id, price FROM flats')
expensive_flats = 0
for k1, flat in enumerate(flats):
    price = int(flat[1].split('.')[0])
    id_ = flat[0]
    if price > 500000:
        print(price)
        expensive_flats = expensive_flats + 1
        cursor2.execute("DELETE FROM flats WHERE id LIKE ?", (id_,))
print(expensive_flats)
conn.commit()


##############
# Other cities
##############

conn = sqlite3.connect("data//2019.06//cian_other_flats.db")
cursor = conn.cursor()
cursor2 = conn.cursor()

# Others
cursor.execute("""CREATE TABLE IF NOT EXISTS flats
                  (id text primary key, rooms text, type text, metro text, address text, area text,
                  house text, parking text, price text, phones text, description text, repair text,
                  area_rooms text, balcony text, windows text, toilet text, children_animals text,
                  additional text, zhk text, series text, ceiling text, lift text, chute text,
                  link text)
               """)
conn.commit()

spb_string_1 = 'Санкт-Петерб%'
spb_string_2 = 'Лен%'
moscow_strings = 'Моск%'

# Delete records related to Msk and Spb
cursor.execute("DELETE FROM flats WHERE address LIKE (?) OR address LIKE (?) OR address LIKE (?)",
               (spb_string_1, spb_string_2, moscow_strings))
conn.commit()

# Delete records of more than 500'000 per month costs (mistakes)
flats = cursor.execute('SELECT id, price FROM flats')
expensive_flats = 0
for k1, flat in enumerate(flats):
    price = int(flat[1].split('.')[0])
    id_ = flat[0]
    if price > 500000:
        print(price)
        expensive_flats = expensive_flats + 1
        cursor2.execute("DELETE FROM flats WHERE id LIKE ?", (id_,))
print(expensive_flats)
conn.commit()

# # Make one largest table
# conn = sqlite3.connect("data//2019.06//cian_spb_flats.db")
# cursor = conn.cursor()
# cursor.execute("ATTACH DATABASE ? AS cian_moscow_flats", ("data/cian_moscow_flats.db",))
# cursor.execute("INSERT OR IGNORE INTO flats SELECT * FROM cian_moscow_flats.flats;")
