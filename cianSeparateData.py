import sqlite3
from xlsxwriter.workbook import Workbook


# Export tables to xlsx
def export_to_xlsx(city, good_or_bad):
    db = 'data//2019.06//cian_' + city + '_flats.db'
    xlsx = 'data//2019.06//cian_' + city + '_' + good_or_bad + '.xlsx'
    workbook = Workbook(xlsx)
    worksheet = workbook.add_worksheet()

    conn2 = sqlite3.connect(db)
    c = conn2.cursor()
    statement = 'SELECT * FROM ' + good_or_bad + ' '
    output_table1 = c.execute(statement)
    for k1, row1 in enumerate(output_table1):
        for m, value1 in enumerate(row1):
            worksheet.write(k1, m, value1)
    workbook.close()


def add_good_bad_tables(city):
    db = 'data//2019.06//cian_' + city + '_flats.db'
    conn = sqlite3.connect(db)
    cursor = conn.cursor()

    cursor.execute("""CREATE TABLE IF NOT EXISTS bad
                      (id text primary key, rooms text, type text, metro text, address text, area text,
                      house text, parking text, price text, phones text, description text, repair text,
                      area_rooms text, balcony text, windows text, toilet text, children_animals text,
                      additional text, zhk text, series text, ceiling text, lift text, chute text,
                      link text)
                    """)
    conn.commit()
    cursor.execute("""CREATE TABLE IF NOT EXISTS good
                      (id text primary key, rooms text, type text, metro text, address text, area text,
                      house text, parking text, price text, phones text, description text, repair text,
                      area_rooms text, balcony text, windows text, toilet text, children_animals text,
                      additional text, zhk text, series text, ceiling text, lift text, chute text,
                      link text)
                    """)
    conn.commit()

    # Make separate table for discriminated
    # discrimination_list_old = ['%славян%', '%кавказ%', '%русск%', '%рф%', "%росси%", "%российск%", "% ази%"]
    discrimination_list = ['%славян%', '%кавказ%', '%русск%', '%рф%', "%росси%", "%российск%", "% ази%", '%СНГ%',
                           '% азер%', '%молдов%', '% хохл%', '%национальн%', '%киргиз%', '%казах%', '%армян%',
                           '%беженц%', '%беженец%', '%беженк%', '% сири%']

    for word in discrimination_list:
        cursor.execute("PRAGMA case_sensitive_like = OFF")
        cursor.execute("SELECT * FROM flats "
                       "WHERE description LIKE ?", (word,))
        value = cursor.fetchall()
        print(len(value))
        for i in value:
            cursor.execute("INSERT OR IGNORE INTO bad VALUES "
                           "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                           i)
            conn.commit()
    conn.commit()

    # Make separate tables for non-discriminated
    cursor.execute("INSERT OR IGNORE INTO good SELECT * FROM flats")
    conn.commit()
    conn3 = sqlite3.connect(db)
    c2 = conn3.cursor()
    c3 = conn3.cursor()
    output_table_2 = c2.execute("SELECT * FROM bad ")
    for k, row in enumerate(output_table_2):
        print(k)
        c3.execute("DELETE FROM good WHERE id = ?", (row[0],))
    conn3.commit()


add_good_bad_tables('moscow')
add_good_bad_tables('spb')
add_good_bad_tables('all')
add_good_bad_tables('other')

# export_to_xlsx('moscow', 'good')
# export_to_xlsx('moscow', 'bad')
# export_to_xlsx('moscow', 'flats')
# export_to_xlsx('spb', 'bad')
# export_to_xlsx('spb', 'good')
# export_to_xlsx('spb', 'flats')
# export_to_xlsx('all', 'good')
# export_to_xlsx('all', 'bad')
# export_to_xlsx('all', 'flats')
export_to_xlsx('other', 'good')
export_to_xlsx('other', 'bad')
export_to_xlsx('other', 'flats')
