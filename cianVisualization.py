import sqlite3
import pandas
import random
import statistics
from matplotlib import pyplot
from wordcloud import WordCloud, STOPWORDS
from xlsxwriter.workbook import Workbook
import matplotlib.pyplot as plt


def export_to_xlsx(list1):
    workbook = Workbook('good.xlsx')
    worksheet = workbook.add_worksheet()

    for i in range(0, len(list1)):
        worksheet.write(i, 0, list1[i])
    workbook.close()


def grey_color_func():
    return "hsl(0, 0%%, %d%%)" % random.randint(0, 10)


def make_wordcloud():
    conn1 = sqlite3.connect("data//2019.04//cian_moscow_flats.db")
    c1 = conn1.cursor()
    output_table1 = c1.execute("SELECT * FROM bad ")
    stopwords = set(STOPWORDS)
    words = []
    for k1, row1 in enumerate(output_table1):
        words.append(row1[10])
        print(row1[10])
    title = "Так говорят ксенофобы"
    word_cloud = WordCloud(
        font_path='//fonts//Roboto-Black.ttf',
        background_color='white',
        stopwords=stopwords,
        max_words=200,
        max_font_size=30,
        scale=5,
        random_state=1
    ).generate(str(words))
    fig = plt.figure(1, figsize=(100, 100))
    plt.axis('off')
    if title:
        fig.suptitle(title, fontsize=40)

    plt.imshow(word_cloud.recolor(color_func=grey_color_func, random_state=3),
               interpolation="bilinear")
    plt.show()


conn = sqlite3.connect("data//2019.05//cian_moscow_flats.db")
c = conn.cursor()
output_table = c.execute("SELECT * FROM bad ")
prices = []
for k, row in enumerate(output_table):
    price = row[8]
    price = price.split('.')[0]
    prices.append(price)
print(prices)
conn = sqlite3.connect("data//2019.05//cian_moscow_flats.db")
c = conn.cursor()
output_table_2 = c.execute("SELECT * FROM good ")
prices2 = []
for k, row in enumerate(output_table_2):
    price2 = row[8]
    price2 = price2.split('.')[0]
    prices2.append(price2)
print(prices2)

export_to_xlsx(prices2)

class1 = ['0'] * len(prices)
class2 = ['1'] * len(prices2)
classes = class1
classes.extend(class2)
prices_all = prices
prices_all.extend(prices2)

prices = [float(s) for s in prices]
prices2 = [float(s) for s in prices2]
print(statistics.mean(prices))
print(statistics.mean(prices2))
print(prices)
print(prices2)

pyplot.hist([prices, prices2], bins=1500, alpha=1, color=['#ffcd00', '#000000'],
            label=['Доступно славянам', 'Доступно всем'])
pyplot.legend(loc='upper right')
pyplot.xlim(0, 120000)
pyplot.ylabel("Количество объявлений")
pyplot.xlabel("Цена аренды в месяц")

print(len(classes))
print(len(prices_all))
data_frame = pandas.DataFrame(
    {'class': classes,
     'price': prices_all,
     })
print(data_frame)
