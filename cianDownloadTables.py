from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium import webdriver

profile = webdriver.FirefoxProfile()
profile.set_preference("browser.download.folderList", 2)
profile.set_preference("browser.download.manager.showWhenStarting", False)
profile.set_preference("browser.download.dir", 'D:\\Projects\\Python\\CianParser\\data\\Tables_Msk\\')
profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                       "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/x-excel, "
                       "application/x-msexcel, application/excel, application/vnd.ms-excel")

opts = Options()
opts.headless = True
assert opts
browser = Firefox(options=opts, firefox_profile=profile)
# Moscow 1-382
# NN 450-511
# Tyumen 1370-1462
# Spb 1-232
N1 = 1
N2 = 500

for i in range(1, 490):

    by_metro = "https://www.cian.ru/cat.php?deal_type=rent&engine_version=2&metro%5B0%5D=" + str(i) + \
              "&offer_type=flat&room1=1&room2=1&room3=1&room4=1&room5=1&room6=1&room7=1&room9=1&type=4"
    browser.get(by_metro)
    get_xls_button = browser.find_elements_by_class_name('_93444fe79c--light--366j7')
    get_xls_button[0].click()

profile.set_preference("browser.download.dir", 'D:\\Projects\\Python\\CianParser\\data\\Tables_Spb\\')
opts = Options()
opts.headless = True
assert opts
browser = Firefox(options=opts, firefox_profile=profile)

for i in range(1, 500):

    by_metro = "https://spb.cian.ru/cat.php?deal_type=rent&engine_version=2&metro%5B0%5D=" + str(i) + \
                  "&offer_type=flat&room1=1&room2=1&room3=1&room4=1&room5=1&room6=1&room9=1&type=4"
    browser.get(by_metro)
    get_xls_button = browser.find_elements_by_class_name('_93444fe79c--light--366j7')
    get_xls_button[0].click()
